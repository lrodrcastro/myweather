def windDirection(deg):
    cardinal_directions = ['North', 'Northeast',
                           'East', 'Southeast', 'South',
                           'Southwest', 'West', 'Northwest']
    direction = int(deg/45)
    return (str(int(deg)) + "° " + cardinal_directions[direction])