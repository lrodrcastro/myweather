import requests, datetime, zipcodes
from ipware import get_client_ip
from django.shortcuts import render
from math import ceil, floor
from .forms import CityForm
from .models import CityForecast
from .wind import windDirection
from .weatherIcon import weatherIcon

# Create your views here.
zipcode = None

def home(request):
    ''' Weather Portion '''
    url = 'http://api.openweathermap.org/data/2.5/forecast?zip={},us&units=imperial&appid=29e36227fc9e25d93b75f0a65016cf00'
    #Client IP address geolocation for initialization
    ip, is_routable = get_client_ip(request)
    if ip is not None and is_routable:
        geolocation_url = 'http://api.ipstack.com/{}?access_key=dec671d5cb176415bd110c7a57c79876&format=1'
        r = requests.get(geolocation_url.format(ip)).json()
        zipcode = r['zip']
        r = requests.get(url.format(zipcode)).json()
        form = CityForm()    
    else:
        r = requests.get(url.format('11101')).json()
        form = CityForm()
    
    if request.method == 'POST':
        form = CityForm(request.POST)
        try:
            if form.is_valid() and zipcodes.is_valid(form.instance.zipcode):
                zipcode = form.instance.zipcode
                r = requests.get(url.format(zipcode)).json()
        except:
            print ("Zipcode is not valid!")
            
                
    # JSON pull
    # API records weather in 3 hour intervals
    # Loop jumps by 8 to index API accordingly
    city_forecast = {'city_name': r['city']['name'],
                     'wind_speed': r['list'][0]['wind']['speed'],
                     'wind_direction': windDirection(r['list'][0]['wind']['deg']),
                    } 
    day = 0
    for index in range(0, 33, 8):
        city_forecast.update({
            'temp_max' + str(day): ceil(r['list'][index]['main']['temp_max']),
            'temp_min' + str(day): floor(r['list'][index]['main']['temp_min']),
            'icon' + str(day): r['list'][index]['weather'][0]['main']
        })
        day = day + 1

    
    # Dictionary for today and 4 proceeding days
    date_time = { 'time': datetime.datetime.now() }
    for date in range(0, 5):
        date_time.update({'day' + str(date): datetime.datetime.now() + datetime.timedelta(date)})
        
        
    ''' News Portion '''
    url = 'https://newsapi.org/v2/top-headlines?country=us&sortBy=popularity&apiKey=6c81c18cc7c7491e8bd867e1002015fb' 
    r = requests.get(url).json()
    
    news = {}
    for index in range(3):
        news.update({
            'headline' + str(index): r['articles'][index]['title'],
            'description' + str(index): r['articles'][index]['description'],
            'date' + str(index): r['articles'][index]['publishedAt'],
            'news_url' + str(index): r['articles'][index]['url']
        })
     
    context = {'city' : city_forecast,
               'date_time': date_time,
               'form': form,
               'news': news}
              
    return render(request, 'home.html', context)

def news(request):
    url = ('https://newsapi.org/v2/top-headlines?sources=google-news&sortBy=popularity&apiKey=6c81c18cc7c7491e8bd867e1002015fb')   
    r = requests.get(url).json()
    
    news = {}
    for index in range(3):
        news.update({
            'headline' + str(index): r['articles'][index]['title'],
            'description' + str(index): r['articles'][index]['description'],
            'date' + str(index): r['articles'][index]['publishedAt'],
            'news_url' + str(index): r['articles'][index]['url'],
            'urlToImage' + str(index): r['articles'][index]['urlToImage']
        })
    
    context = {'news': news}
    return render(request, 'news.html', context)

def contact(request):
    return render(request, 'contact.html')