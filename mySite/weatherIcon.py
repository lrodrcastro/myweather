def weatherIcon(weather):
    lst = ['Clear', 'Rain', 'Cloudy']
    if weather in lst:
        return (weather + ".svg")
    else:
        return ("Clear.svg")