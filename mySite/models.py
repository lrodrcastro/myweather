from django.db import models
from django import forms


# Create your models here.
class CityForecast(models.Model):
    zipcode = models.CharField(max_length=5)
    
    #wind_speed = models.IntegerField()
    #wind_direction = models.IntegerField()
    
    #temp_max0 = models.IntegerField()
    #temp_max1 = models.IntegerField()
    #temp_max2 = models.IntegerField()
    #temp_max3 = models.IntegerField()
    #temp_max4 = models.IntegerField()

    #temp_min0 = models.IntegerField()
    #temp_min1 = models.IntegerField()
    #temp_min2 = models.IntegerField()
    #temp_min3 = models.IntegerField()
    #temp_min4 = models.IntegerField()
    
    def __str__(self):
        return self.zipcode
    
    class Meta:
        verbose_name_plural = 'cities'
    
