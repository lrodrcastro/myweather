from django.forms import ModelForm, TextInput
from .models import CityForecast

class CityForm(ModelForm):
    class Meta:
        model = CityForecast
        fields = ['zipcode']
        widgets = {'zipcode': TextInput(attrs={'class': 'input', 'placeholder': 'Enter your zip code here...'})}
        
